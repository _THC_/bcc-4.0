
# The Moviex Project #

## About ##

Django web application built for the Best Code Challenge 4.0 competition.

Web application consists of a movie database and recommends movies to a user based on his movie interests.

## Prerequisites ##

- Python 2.6 or 2.7
- pip
- virtualenv (virtualenvwrapper is recommended for use during development)
- django

## Installation ##

Running django application requires django installation: https://docs.djangoproject.com/en/dev/intro/install/
When django is installed, position yourself to the same directory as manage.py file.
Run from console: python manage.py runserver

Application should be made available at http://localhost:80


License
-------
This software is licensed under the [New BSD License][BSD]. For more
information, read the file ``LICENSE``.

[BSD]: http://opensource.org/licenses/BSD-3-Clause
