""" Views for the base application """

from django.shortcuts import render
from django.http import HttpResponse
from base.models import Movie
from django.template import RequestContext, loader
import Levenshtein
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from recommender import *
import json

# when the app starts load the recommendation model to memory
loadModel()

searchquerry_key    = "searchtext"
searchresult_key    = "searchresult"
allmovies_key       = "movies"
singlemovie_key     = "movie"
movie_metadata_key  = "metadata"
sessionmovies_key   = "clicked_movies"
recommended_key     = "recommended"
searchresult_max    = 16
paging_max          = 20

def home(request):
    """ Default view for the root """

    template = loader.get_template('base/home.html')
    context = RequestContext(request, {})

    movies = Movie.objects.all()

    context[recommended_key] = get_recommended_movies(request)
    
    # process search querry
    if request.POST and request.POST[searchquerry_key]:

        searchtext = request.POST[searchquerry_key]
        searchtext_parts = searchtext.strip().split(' ')
        closestmovies = []

        for movie in movies:
            titleparts = movie.title.strip().split(' ')
            totalratio = 0

            # calculate levenshtein ratio for every movie
            for searchword in searchtext_parts:
                maxratio = 0
                for titleword in titleparts:
                    levenshteinratio = Levenshtein.ratio(searchword.lower(),
                                                         titleword.lower())
                    maxratio = max(levenshteinratio, maxratio)
                totalratio += maxratio

            closestmovies.append([movie, totalratio])

        # sort all movies by levenshtein ratio
        closestmovies.sort(key=lambda m: m[1], reverse=True)
        
        # removes levenshtein ratios from movies
        closestmovies = [pair[0] for pair in closestmovies]
        # get top X movies
        closestmovies = closestmovies[:searchresult_max]

        # pack closest movies in context and pass it to template
        context[searchquerry_key] = searchtext
        context[searchresult_key] = closestmovies
        return HttpResponse(template.render(context))

    # Show 25 movies per page
    paginator = Paginator(movies, paging_max)

    page = request.GET.get('page')
    try:
        movies = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        movies = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        movies = paginator.page(paginator.num_pages)

    context[allmovies_key] = movies
    return HttpResponse(template.render(context))

def details(request, movie_id):

    template = loader.get_template('base/movie.html')
    context = RequestContext(request, {})

    movie = Movie.objects.get(id=movie_id)

    if sessionmovies_key not in request.session.keys():
            request.session[sessionmovies_key] = []
    request.session[sessionmovies_key].append(movie_id)
    request.session.save()

    context[singlemovie_key] = movie
    context[movie_metadata_key] = json.loads(movie.metadata)
    context[recommended_key] = get_recommended_movies(request)

    return HttpResponse(template.render(context))

def get_recommended_movies(request):
    clicked_movies = []
    recommended_movies = []
    
    if sessionmovies_key not in request.session.keys():
        request.session[sessionmovies_key] = []

    # get all previously clicked movies
    for clicked_id in set(request.session[sessionmovies_key]):
        # clicked_movies.append(Movie.objects.get(id=clicked_id))
        clicked_movies.append(int(clicked_id))

    # recommend movie id's based on user clicks
    if clicked_movies:
        recommended_movies = recommend(clicked_movies)

    # prepare recommendation data for display
    recommended_movies = [Movie.objects.get(id=i) for i in recommended_movies]

    return recommended_movies