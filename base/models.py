""" Basic models, such as user profile """
from django.db import models

class Movie(models.Model):
    title 		= models.CharField(max_length=255)
    poster_path = models.CharField(max_length=255)
    metadata = models.TextField()
    slug = models.CharField(max_length=255)
    
    def __str__(self):
        return self.title

