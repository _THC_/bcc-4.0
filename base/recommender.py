import time
import json
import math
import scipy.spatial.distance as sc
import operator
import os

from util import *

# least number of users a movie should share with another movie
# so they could be compared
USER_LOWER_BOUND = 10

# list containing all of the movie titles
movie_list = [0]

# stores all the movies the user has rated
# eg. userID: { movieID1: 5, movieID2: 1, ... }
user_ratings = dict()

# for each movie, stores a list of movies sorted by similarity
# with the most similar at the front
movie_similarity = dict()

# for each movie, stores the list of users that rated that movie
# eg. movieID -> userID1, userID2, userID3, ...
movies_rated_by = dict()

# index values that will be used for movieID and the similarity value
MOVIE_ID = 0
SIMILARITY_ID = 1

DEBUG = False
# DEBUG = True

def loadData():

	""" Loads .csv file data to memory """

	# load movie title data to memory
	fmovie = open(FILE_MOVIES)
	for line in fmovie.readlines():
		line = line.rstrip().split(",")

		identifier = line[0]
		title = line[1]

		movie_list.append(title)

	# load movie rating data to memory
	fpref = open(FILE_PREFS)
	lines = fpref.readlines()

	for line in lines:

		(userID, movieID, rating) = parseLine(line)

		try:
			# put user data into memory
			user_ratings[userID][movieID] = rating
		except KeyError:
			# dictionary was not initialized
			user_ratings[userID] = dict()
			user_ratings[userID][movieID] = rating

		try:
			# add user to the list of users that rated this movie
			movies_rated_by[movieID].append(userID)
		except:
			# user list was not initialized
			movies_rated_by[movieID] = list()
			movies_rated_by[movieID].append(userID)

def averageRating(userID):
	""" Calculates the average rating a user has given """
	movies = user_ratings[userID]
	Ravg = 0.0 
	for movie, rating in movies.items():
		Ravg += 1.0 * rating			
	Ravg /= len(user_ratings[userID])
	return Ravg

def cosine(ratings1, ratings2):
	return sc.cosine(ratings1, ratings2)

def correlation(ratings1, ratings2):
	return sc.correlation(ratings1, ratings2)

def similarity(movieID1, movieID2):

	"""
	Calculates the correlation between two movies
	effectively calculating how similar they are, according 
	to user ratings.
	"""

	try:
		# extract list of users that rated each movie
		movie1_users = movies_rated_by[movieID1]
		movie2_users = movies_rated_by[movieID2]
	except:
		return NEG_INF


	# find the intersection of the two lists of users, in other words,
	# find all users that have rated both movies
	movie_users = intersect(movie1_users, movie2_users)

	ratings1 = []
	ratings2 = []

	if len(movie_users) < USER_LOWER_BOUND:
		return NEG_INF

	# calculate the sum of the products of rating deviation
	for user in movie_users:
		ratings1.append(user_ratings[user][movieID1])
		ratings2.append(user_ratings[user][movieID2])

	try:

		result = correlation(ratings1, ratings2)
		# result = sc.cosine(ratings1, ratings2)

		if math.isnan(result):
			return NEG_INF

		return result
	except:
		return NEG_INF

def ratingPrediction(userID, movieID, K = 500):

	""" Predicts the rating a user will give to a movie using weighted sum principle. """
	
	total_similarity_sum = 0
	weighted_sum = 0

	similarities = getSimilarItems(movieID)

	threshold = 0.0

	# print similarities

	# calculate the sum of all similarities weighted by the rating and divided
	# by the total sum of the absolute similarity value
	for e in similarities:
		movie_identifier = e[MOVIE_ID]
		sim = e[SIMILARITY_ID]

		if sim < threshold:
			break

		# we have exhausted our neighbours
		if K == 0:
			break

		# a movie should not influence a prediction on itself
		if movie_identifier == movieID:
			continue

		try:
			rating = user_ratings[userID][movie_identifier]
			weighted_sum += sim * rating
			total_similarity_sum += abs(sim)

			if DEBUG:
				print movie_list[movie_identifier], "     Rating: ", rating, sim, K
				print "Prediction: ", weighted_sum / total_similarity_sum
				print

			K -= 1

		except:
			pass

	# make sure we don't divide by 0
	if total_similarity_sum == 0:
		return 0

	if DEBUG:
		print "Weighted sum: ", weighted_sum
		print "Total sum: ", total_similarity_sum

	return weighted_sum / total_similarity_sum


def getSimilarItems(movieID):

	"""
	Calculates and returns a list of movies sorted by similarity
	"""

	try:
		return movie_similarity[int(movieID)]
	except:
		pass
	
	similarities = []
	for idx in range(1, len(movie_list)):

		# dont't check if a movie is similar to itself
		if idx == movieID:
			continue

		# calculate the similarity
		movie_similarity_value = similarity(movieID, idx)		

		# add the movieID and the similarity value to a list of similarities
		similarities.append((idx, movie_similarity_value))
	
	# sort the movies by similarity, the most similar movie at the front
	similarities = list(reversed(sorted(similarities, key = lambda t: t[1] )))
	
	return similarities

def trainModel():
	""" Calculates the similarities for all movies """

	# calculate the similarities for all movies
	for idx in range(1, len(movie_list)):
		movie_similarity[idx] = getSimilarItems(idx)

	# output the model to a file
	with open(FILE_MODEL, 'wb') as fp:
		fp.write(json.dumps(movie_similarity, indent=2, sort_keys = True))

def testModel():
	""" Tests how well does the model predict user ratings """

	# open the file where the model is stored
	test_set = open(FILE_TEST_SET, "r")
	lines = test_set.readlines()

	N = len(lines)

	# init values
	prediction_error = 0
	diff = -1000000

	for line in lines:
		(userID, movieID, rating) = parseLine(line)

		# (userID, movieID, rating) = (545, 542, 2)

		# make the actual prediction using weighted sum
		predicted = int(round(ratingPrediction(userID, movieID)))

		# check for the biggest prediction difference
		diff = max(abs(predicted - rating), diff)

		if predicted != rating:
			# rating prediction and the actual rating don't match
			prediction_error += 1

			if DEBUG:
				if predicted != 0 and abs(predicted - rating) > 0:
					print userID, movieID, movie_list[movieID], "Rating: ", rating, "Predicted: ", predicted
					break
			
	# output the results
	print "Max diff: ", diff
	print prediction_error, N
	print "Total error: ", prediction_error * 1.0 / N



def loadModel():
	""" Loads the model data from file to memory """

	global movie_similarity

	filename = os.path.join(os.path.dirname(os.path.realpath(__file__)), FILE_MODEL)

	# load model from a file
	with open(filename, "r") as fmodel:
		movie_similarity = json.load(fmodel)

	# convert movie id keys from string to int
	moved_identifiers = movie_similarity.keys()
	for movieID in moved_identifiers:
		movie_similarity[int(movieID)] = movie_similarity[movieID]
		# delete old data
		del movie_similarity[movieID]

def topNRecommendation(movieID, visited_movies, topN_movies, N):

	""" Returns the top N recommended movies according to what the user has already visited """

	# get all items from the similarity list
	all_similar_items = movie_similarity[movieID]

	similar_items = []
	idx = 0
	offset = 0

	# only add movies that were not visited by the user
	while idx < N + offset:
		if all_similar_items[idx][0] not in visited_movies:
			similar_items.append(all_similar_items[idx])
		else:
			offset += 1

		# the user has seen all movies so there is nothing to recommend
		if offset == len(movie_similarity):
			break
		
		idx += 1

	recommended_movies = dict()
	for item in similar_items:
		recommended_movies[item[MOVIE_ID]] = item[SIMILARITY_ID]

		try:
			topN_movies[item[MOVIE_ID]] += item[SIMILARITY_ID]
		except:
			topN_movies[item[MOVIE_ID]] = item[SIMILARITY_ID]

	# return a dictionary of top n movies with their corresponding similarity values
	return recommended_movies


def recommend(movies, N = 10):

	# check if the model is loaded to memory
	if len(movie_similarity) == 0:
		loadModel()

	topN_movies = dict()

	# make a recommendation list for all movies a user has visited
	for movie in movies:
		topNRecommendation(movie, movies, topN_movies, N)

	# sort results by most similar and extrapolate data from the dictionary
	sorted_recommendation = list(reversed(sorted(topN_movies.iteritems(), \
											     key=operator.itemgetter(1))))

	# finally, select top N movie IDs that were generated from the algorithm
	final_recommendation = []
	for idx in range(N):
		(movieID, sim) = sorted_recommendation[idx]
		final_recommendation.append(movieID)

	return final_recommendation


if __name__ == "__main__":

	""" DEBUG """

	start_time = time.time()

	loadData()
	trainModel()
	loadModel()
	testModel()

	# print recommend([100, 200, 1066, 1153])

	print time.time() - start_time, "seconds"
	