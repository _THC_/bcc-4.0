
FILE_MOVIES = "movies.csv"
FILE_PREFS = "prefs.csv"
FILE_TEST_SET = "test-set.txt"
FILE_MODEL = "model.json"

NEG_INF = -100000

def union(a, b):
    """ return the union of two lists """
    return list(set(a) | set(b))

def intersect(a, b):
	""" return the intersection of two lists """
	return list(set(a) & set(b))

def parseLine(line):
	line = line.rstrip().split(",")

	# extract user data
	userID = int(line[0])
	movieID = int(line[1])
	rating = int(line[2])

	return (userID, movieID, rating)