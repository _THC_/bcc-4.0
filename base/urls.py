"""urlconf for the base application"""

from django.conf.urls import url, patterns, include
from base import views


urlpatterns = patterns('base.views',
    url(r'^$', views.home, name='home'),
    url(r'^details/(?P<movie_id>\d+)/$', views.details, name='details'),
)
