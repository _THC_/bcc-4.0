import json
from util import *


THE_DELIMITER = ", The"
A_DELIMITER = ", A"
IL_DELIMITER = ", Il"

INITIAL_DATA_FILE = "../fixtures/initial_data.json"


def splitter(line, delimiter):

	element = THE_DELIMITER.split(", ")[1] + " "

	line = line.rstrip().split(delimiter)
	line = element + line[0].split(",", 1)[1]

	return line


def generateInitialData(movies):

	initialData = []

	for idx in range(len(movies)):
		element = dict()

		element["model"] = "base.model.Movie"
		element["pk"] = idx + 1
		element["fields"] = dict()
		element["fields"]["title"] = movies[idx]

		# print idx + 1, movies[idx]
		# raw_input()

		initialData.append(element)

	with open(INITIAL_DATA_FILE, 'wb') as fp:
		fp.write(json.dumps(initialData, indent=2, sort_keys = True))

	# print initialData


if __name__ == "__main__":


	fmovie = open(FILE_MOVIES, "r")
	lines = fmovie.readlines()

	movies = []

	for line in lines:

		splitter(line, THE_DELIMITER)

		if line.find(THE_DELIMITER) != -1:
			line = splitter(line, THE_DELIMITER)
		elif line.find(A_DELIMITER) != -1:
			line = splitter(line, A_DELIMITER)
		elif line.find(IL_DELIMITER) != -1:
			line = splitter(line, IL_DELIMITER)
		else:
			line = line.rstrip().split(",", 1)
			line = line[1]

		movies.append(unicode(line, errors='replace'))

	generateInitialData(movies)
